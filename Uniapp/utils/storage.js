let systemConfigName = 'system_config'
let sendCodeTimeName = 'sendCodeTimes'

let loginStorageName = 'loginStorage'


export const setSystemConfig = (data)=>{
    uni.setStorageSync(systemConfigName,JSON.stringify(data))
}
export const getSystemConfig = ()=>{
    let jsonStr = uni.getStorageSync(systemConfigName)
    if(jsonStr) return JSON.parse(jsonStr)
    return undefined
}

export const getAliPayConfig = ()=>{
    let data = getSystemConfig()
    if(data){
        return data['alipay_status']
    }else{
        return '0'
    }
}

export const getHighDrawConfig = ()=>{
    let data = getSystemConfig()
    if(data){
        return data['paint_draw_status']
    }else{
        return '0'
    }
}

export const getisCanRecordConfig = ()=>{
    let data = getSystemConfig()
    if(data){
        return data['tencentai_status']
    }else{
        return '0'
    }
}

export const setSendCode = ()=>{
    let temp = uni.getStorageSync(sendCodeTimeName)
    console.log(temp)
    if(temp){
        let temp1 = JSON.parse(temp)
        console.log(temp1,'ccccc')
        if(temp1.length==5){
            // 如果首个是在今天，则不可以再发
            
            if(new Date().toDateString()==temp1[0]){
                return 
            }else{
                // 删掉第一个，再加上一个，可继续发
                temp1.splice(0,1)
                temp1.push(
                   new Date().toDateString()
                )
            }
        }else{
            temp1.push(
                new Date().toDateString()
            )
        }

        uni.setStorageSync(sendCodeTimeName,JSON.stringify(temp1))
    }else{
        let temp = [new Date().toDateString()]
        uni.setStorageSync(sendCodeTimeName,JSON.stringify(temp))
    }
   
    
}
export const sendCodeFail=()=>{
    let temp = uni.getStorageSync(sendCodeTimeName)
    if(temp){
        let temp1 = JSON.parse(temp)
        temp1.splice(temp.length-1,1)
        uni.setStorageSync(sendCodeTimeName,JSON.stringify(temp1))
    }
}

export const isCanSendCode=()=>{
    let temp = uni.getStorageSync(sendCodeTimeName)
   
    if(temp){
        let temp1 = JSON.parse(temp)
        if(temp1.length==5){
            // 如果首个是在今天，则不可以再发
            if(new Date().toDateString()==temp1[0]){
                return false
            }
        }
    }
    return true
}

export const isCanLogin=()=>{
    let temp = uni.getStorageSync(loginStorageName)
   
    if(temp){
        let temp1 = JSON.parse(temp)
        if(temp1.length==5){
            // 如果首个是在今天，则不可以再发
            if(new Date().getTime()-temp1[0]<1000*60*60){
                return false
            }
        }
    }
    return true
}

export const setLoginTimes = ()=>{
    let temp = uni.getStorageSync(loginStorageName)
    console.log(temp)
    if(temp){
        let temp1 = JSON.parse(temp)
        console.log(temp1,'ccccc')
        if(temp1.length==5){
            // 如果首个是在今天，则不可以再发
            
            if(new Date().getTime()==temp1[0]){
                return 
            }else{
                // 删掉第一个，再加上一个，可继续发
                temp1.splice(0,1)
                temp1.push(
                   new Date().getTime()
                )
            }
        }else{
            temp1.push(
                new Date().getTime()
            )
        }

        uni.setStorageSync(loginStorageName,JSON.stringify(temp1))
    }else{
        let temp = [new Date().getTime()]
        uni.setStorageSync(loginStorageName,JSON.stringify(temp))
    }
   
    
}
