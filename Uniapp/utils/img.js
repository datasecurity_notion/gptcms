
function getScreenWidth() {
      uni.getSystemInfo({
          success: function (e) {
            uni.setStorageSync('screenHeight',e.windowHeight)
            if(e.windowWidth>700){
              uni.setStorageSync('screenWidth',700)
            }else{
              uni.setStorageSync('screenWidth',e.windowWidth)
            }
              console.log(e.statusBarHeight,'ssssss')
              let statusHeight = e.statusBarHeight
              // #ifdef H5
              statusHeight = 0
              // #endif
              uni.setStorageSync('statusBarHeight',statusHeight)
              return e;
          }
      })
}
function download(src){
  uni.downloadFile({
    url: src,
    success: (res) => {
      uni.hideLoading();
      if (res.statusCode === 200) {
        uni.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function() {
            uni.showToast({
              title: "保存成功",
              icon: "none"
            });
          },
          fail: function() {
            uni.showToast({
              title: "保存失败，请稍后重试",
              icon: "none"
            });
          }
        });
      }
    },
    fail: (err) => {
      uni.showToast({
        title: "下载失败",
        icon: "none"
      });
    }
  })
}
function saveImg(src){
  uni.authorize({
    scope: 'scope.writePhotosAlbum',
    success: () => {
                // 已授权
                uni.showLoading({
                  title: '加载中'
                });
             download(src)
                  
    },
    fail: () => {
                // 拒绝授权，获取当前设置
      uni.getSetting({
        success: (result) => {
          if (!result.authSetting['scope.writePhotosAlbum']) {
            uni.showModal({
              content: '由于您还没有允许保存图片到您相册里,无法进行保存,请点击确定允许授权',
              success: (res) => {
                if (res.confirm) {
                  uni.openSetting({
                    success: (result) => {
                      console.log(result.authSetting,'asdasdasd');
                    }
                  });
                }
              }
            });
          }
        }
      });
    }
  })
}


export default {
	getScreenWidth,
  saveImg,
  download
}