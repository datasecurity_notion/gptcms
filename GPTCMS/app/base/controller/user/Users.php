<?php 
// +----------------------------------------------------------------------
// | 狂团[kt8.cn]旗下KtAdmin是为独立版SAAS系统而生的快速开发框架.
// +----------------------------------------------------------------------
// | [KtAdmin] Copyright (c) 2022 http://ktadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\base\controller\user;
use think\facade\Db;
use app\base\controller\BaseUser;
use Ramsey\Uuid\Uuid;
use think\facade\Session;
use think\facade\Cache;
use app\base\model\BaseModel;

class Users extends BaseUser
{
    public function bindphone()
    {
        $wid = Session::get('wid');
        $user = Db::table('kt_base_user')->find($wid);
        if(!$user) return error('系统错误');
        $phone = $this->req->param("phone");
        $verfiy = $this->req->param('verfiy');
        if(!preg_match("/^1[0-9]{10}/",$phone)) return error('手机号码格式错误');
        $key = 'sms_'.$phone;
        if($verfiy != Cache::get($key)) return error('验证码错误');

        if($user['telephone'] == $phone){
            //解绑手机号
            Db::table('kt_base_user')->save([
                'id' => $user['id'],
                'telephone' => ''
            ]);
            return success('解绑成功');
        }
        $hasuser = Db::table('kt_base_user')->where('telephone',$phone)->find();
        if($hasuser) return error('手机号已被绑定');
        Db::table('kt_base_user')->save([
            'id' => $user['id'],
            'telephone' => $phone
        ]);
        return success('绑定成功');
    }

    public function info()
    {
        $wid = Session::get('wid');
        $user = Db::table('kt_base_user')->find($wid);
        if(!$user) return error('用户不存在');
        return success('用户详情',$user);
    } 

}